<?php
/**
 *
 * @author  Valery Fremaux  valery.fremaux@gmail.com
 * @version 0.0.1
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License, mod/taoresource is a work derived from Moodle mod/resoruce
 * @package mod_sharedresource
 * @subpackage plugin
 *
 */

////////////////////////////////////////////////////////////////////////////////
//  Code fragment to define the subplugin version etc.
//  This fragment is called by /admin/index.php
////////////////////////////////////////////////////////////////////////////////

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2015072700;       // The current module version (Date: YYYYMMDDXX)
$plugin->requires  = 2012062500;       // Requires this Moodle version
$plugin->component  = 'sharedmetadata_suplomfr';       // Requires this Moodle version
$plugin->maturity = MATURITY_RC;     // Full name of the plugin (used for diagnostics)
$plugin->release = '2.7.0 (Build 2015072700)';     // Full name of the plugin (used for diagnostics)
